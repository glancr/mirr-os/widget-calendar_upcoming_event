# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'calendar_upcoming_event/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name = 'calendar_upcoming_event'
  spec.version = CalendarUpcomingEvent::VERSION
  spec.authors = ['Tobias Grasse']
  spec.email = ['tg@glancr.de']
  spec.homepage = 'https://glancr.de'
  spec.summary = 'mirr.OS Widget that displays details for your next calendar event.'
  spec.description = 'Shows details for your next calendar event.'
  spec.license = 'MIT'
  spec.metadata = { 'json' =>
                      {
                        type: 'widgets',
                        title: {
                          enGb: 'Upcoming',
                          deDe: 'Als Nächstes',
                          frFr: 'A venir',
                          esEs: 'Próxima',
                          plPl: 'Nadchodzące',
                          koKr: '곧'
                        },
                        description: {
                          enGb: spec.description,
                          deDe: 'Zeige alle Details deines nächsten Termins.',
                          frFr: 'Afficher tous les détails de votre prochain rendez-vous.',
                          esEs: 'Mostrar todos los detalles de su próxima cita.',
                          plPl: 'Pokaż wszystkie szczegóły dotyczące następnego spotkania.',
                          koKr: '다음 약속의 모든 세부 사항 표시.'
                        },
                        group: :calendar,
                        compatibility: '0.8.0',
                        single_source: false
                      }.to_json }

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'gems.marco-roth.ch'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']
  spec.add_development_dependency 'rails'
  spec.add_development_dependency 'rubocop', '~> 0.81'
  spec.add_development_dependency 'rubocop-rails'
end
