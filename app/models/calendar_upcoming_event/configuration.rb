# frozen_string_literal: true

module CalendarUpcomingEvent
  class Configuration < WidgetInstanceConfiguration
    attribute :date_filter, :integer, default: 5
    attribute :title_filter, :string, default: ""

    validates :date_filter, :title_filter, presence: true
  end
end
