# frozen_string_literal: true

module CalendarUpcomingEvent
  class Engine < ::Rails::Engine
    isolate_namespace CalendarUpcomingEvent
    config.generators.api_only = true
  end
end
